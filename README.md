Sous-fifre: a Discord bot for whatever
======================================

It can draw cards and stuff.


Requirements
------------

* Python >= 3.6


Getting Started
---------------

* Create a Python virtual environment.
```
python3 -m venv env
```

* Upgrade packaging tools.
```
env/*/pip install --upgrade pip setuptools
```

* Install the project in editable mode with its testing requirements.
```
env/*/pip install -e ".[testing]"
```

* Run the project's tests.
```
env/*/pytest
```

* Run the project.

```
env/*/sous-fifre
```


Configuration
-------------

For the bot to connect, a file named `.env` is required at the root of the
project, formatted as such:
```
DISCORD_TOKEN=
BOT_OWNER=
```

The values to the right of the equal signs should be the token to access the
Discord API and the owner's Discord ID. The ID is a large number, not a
nickname. See the Discord API documentation for more help on this topic.
