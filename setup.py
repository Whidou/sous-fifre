import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.md')) as f:
    CHANGES = f.read()

requires = [
    'discord.py',
    'python-dotenv',
]

tests_require = [
    'pytest >= 3.7.4',
    'pytest-cov',
    'flake8 >= 3.8.0',
]

setup(
    name='sous_fifre',
    version='0.0',
    description='Sous-Fifre',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Topic :: Communications :: Chat',
    ],
    author='Whidou',
    author_email='root@whidou.fr',
    url='https://www.whidou.fr/',
    keywords='chat bot discord',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
    },
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'sous-fifre=sous_fifre:main',
        ],
    },
)
