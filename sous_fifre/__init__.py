from .sous_fifre import SousFifre
from dotenv import load_dotenv
from os import getenv

def main():
    """ Run the bot and exit
    """

    # Read environment variables from .env file
    load_dotenv()

    # Create the bot
    bot = SousFifre(getenv('DISCORD_TOKEN'), int(getenv('BOT_OWNER')))

    # Run the bot
    return bot.run()
