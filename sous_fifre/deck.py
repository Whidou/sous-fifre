from random import shuffle



# Major arcanas
ARCANAS = (
    "Le Mat",
    "Le Bateleur",
    "La Papesse",
    "L'Impératrice",
    "L'Empereur",
    "Le Pape",
    "L'Amoureux",
    "Le Chariot",
    "La Justice",
    "L'Ermite",
    "La Roue de Fortune",
    "La Force",
    "Le Pendu",
    "La Mort",
    "Tempérance",
    "Le Diable",
    "La Maison Dieu",
    "L'Étoile",
    "La Lune",
    "Le Soleil",
    "Le Jugement",
    "Le Monde",
)



class Deck:
    """A deck of cards"""

    def __init__(self, *instructions):
        self.cards = []
        self.discard = []
        for instr in instructions:
            for j in instr[1]:
                for i in instr[0]:
                    self.cards.append((i, j))
        self.shuffle()

    def add(self, value):
        """Add a card to the deck by its number"""
        self.cards.append((value, "Atout"))
        self.shuffle()

    def remove(self, value):
        """Remove a card from the deck by its number"""
        for pile in (self.cards, self.discard):
            for card in pile:
                if card[0] == value and card[1] == "Atout":
                    pile.remove(card)
                    return

    def shuffle(self):
        """Shuffle the deck"""
        shuffle(self.cards)

    def draw(self):
        """Draw the first card"""

        # Reshuffle the discard pile into the draw pile if the deck is empty
        if not self.cards:
            self.cards, self.discard = self.discard, self.cards
            self.shuffle()

        # Get the top card and put it in the discard pile
        card = self.cards.pop(0)
        self.discard.append(card)

        # The Fool
        if card[0] == 0 and card[1] == "Atout":
            return "Excuse (Le Mat)"

        # Card value
        if card[1] == "Atout" or 1 < card[0] < 11:
            name = str(card[0])
        elif card[0] == 1:
            name = "As"
        elif card[0] == 11:
            name = "Valet"
        elif card[0] == 12:
            name = "Cavalier"
        elif card[0] == 13:
            name = "Dame"
        elif card[0] == 14:
            name = "Roi"
        else:
            name = str(card[0])

        # Of
        if card[1][0].lower() in "aeiouyéèêôîû":
            name += " d'"
        else:
            name += " de "

        # Card suit
        name += card[1]

        # Format major arcanas
        if card[1] == "Atout" and 0 <= card[0] < len(ARCANAS):
            name += " ({})".format(ARCANAS[card[0]])

        return name

    def __str__(self):
        """Describe the deck"""
        return "{} cards deck {}".format(len(self.cards), self.cards)



if __name__ == "__main__":
    deck = Deck((range(1, 11), ("Trèfle", "Cœur", "Pique", "Carreau")),
                (range(0, 22), ("Atout",)))
    for i in range(80):
        print(deck.draw())
    print(deck)
    deck.add(22)
    print(deck)
    deck.remove(22)
    print(deck)
