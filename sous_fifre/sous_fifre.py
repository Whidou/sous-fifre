from .deck import Deck

from discord import Client

import re



DRAW_RE = re.compile(r"^[!\?\*/]?([Jj])?(?:[Tt]irage|[Rr](?:oll)?|[Cc]arte)$")
ADD_RE = re.compile(r"^[!\?\*/]?[Aa](?:dd|joute?)?\s([0-9]+)$")
REMOVE_RE = re.compile(r"^[!\?\*/]?[Rr]e(?:move|tire|trait)?\s([0-9]+)$")



class SousFifre(Client):
    """ Main bot class
    """

    def __init__(self, token, owner):

        # Values from configuration file
        self.token = token
        self.owner = owner

        # Moderator's deck
        self.moderator = Deck((range(1, 14), ("Épées",)), ((0,), ("Atout",)))
        # Players' deck
        self.players = Deck((range(1, 14), ("Coupes", "Bâtons")))

        # Bot commands
        self.commands = {
            DRAW_RE: self.draw,
            ADD_RE: self.add,
            REMOVE_RE: self.remove,
        }

        return super().__init__()

    def run(self):
        """Run the bot"""
        return super().run(self.token)

    async def on_ready(self):
        """Triggers once the bot is connected"""
        for guild in self.guilds:
            print(
                f'{self.user} is connected to the following guild:\n'
                f'{guild.name}(id: {guild.id})'
            )

    async def on_message(self, message):
        """Triggers when  message is received"""

        # Do not self-trigger
        if message.author == self.user:
            return

        author_id = message.author.id

        # Match the message to a bot command, if any
        answer = None
        for (pattern, cmd) in self.commands.items():
            match = re.match(pattern, message.content)
            if match:
                answer = cmd(author_id, *match.groups())
                break

        if not answer:
            return

        # Send answer
        msg = "<@!{}>: {}".format(author_id, answer)
        await message.channel.send(msg)

    def draw(self, author, j):
        """Draw a card from a deck"""
        if author == self.owner and not j:
            # Moderator's deck
            deck = self.moderator
        else:
            # Players' deck
            deck = self.players

        return deck.draw()

    def add(self, author, number):
        """Add a card to the players' deck by its number"""

        # Only the bot owner can add cards
        if author == self.owner:
            self.players.add(int(number))

    def remove(self, author, number):
        """Remove a card from the players' the deck by its number"""

        # Only the bot owner can remove cards
        if author == self.owner:
            self.players.remove(int(number))
